# herbacous_biomass_plantations

Repository for all things berbaceous biomass blantations.

# BiomassPlantations in the CMIP6 version of the MPI-ESM

This wikipage is about the transfer of Dorothea Mayer's PhD work on herbaceous biomass plantations from the CMIP5 MPI-ESM version to the CMIP6 version, led by Tammas at LMU.

## General idea
Few ESMs have biomass plantations implemented (there are a few more offline DGVMs that do); we'd like to have more than a state-of-the-art model and want to keep Dorothea's implementations alive. Under the 2 degree target NET-scenarios, including BECCS, are paramount.
A CMIP6 MPI-ESM is directly useful for
    - Horizon2020 negative emissions call (discussions started with W/ Buermann in fall 2018)
    - Revisit Harper paper (Harper et al., NatureComm., 2018). In that paper ESMs were run for bioenergy simulations under a 1.5 degree target. A main conclusion of the study is that biomass plantations result in net losses of C. It is doubtful, however, if this conclusion is not flawed by the fact that the ESMs implemented BPs as simple grass or crop, not as a super-efficient tall grass that e.g. miscanthus is. Dorothea's study found a different conclusion, that HBPs outperform forests within decades in many regions. A good application of our new code would thus be to show that a "proper" implementation of HBPs gives very different results from the grass or crop assumption usually used in previous studies.
    - ... [add ideas here as they occur]

## Model implementation
See extra document by Tam describing changes from the merge and to the initial file in detail. Steps taken:

    - merge Dorothea's and CMIP6 code (for now including Dorothea's specific code for HBPs on forest regrowth areas only) -- done
        - [Tam, somewhere we should put down decisions concerning conflicts that were not straightforward (if there were any)]
    - Biomass may not be fully used for substitution, i.e. we want user-defined conversion efficiency (Dorothea's code was commenting in or out lines that allowed either 0% or 100% fossil-fuel substitution). Note that harvest residues have been treated by Dorothea's code well (two values available to choose from), but the carbon losses from harvested material to energy usage had just been addressed with the 0% and 100% assumption before.
        - implement user-defined conversion efficiency -- done N.B. The rate of carbon flux to the atmosphere or eternal carbon pool depends on the amount of harvested carbon for only the current year.
        - find default value or value range for conversion efficiency -- to do, Lena knows a lot here, Dorothea had a couple of references.
    - extended lctlib file -- revisit SPITFIRE parameters, JP to check two phenology parameters
        - [add justification for parameter choices ("same for all grasses and crops", ...; here or elsewhere]
    - extended jsbach initial file (see extra document) -- done(?)
    - try both coupled and offline run technically -- runs coupled and offline (?) but so far only C-only. We eventually aim at running the model in the setup of CMIP6 simulations, i.e. including N.
    - develop way how to check plausibility of results (which variables to look at; under which type of climate) -- Tam started
    - develop setup for ESM run with HBPs in 21st century (see Harper study) -- to do
    - extend LUC transition matrix to HBPs -- todo, follows logic of Reick et al., JAMES, 2013

## Which way is the model supposed to run?

    - As an independent model version, i.e. you can start the model from scratch --> Need to alter initial file and land use forcing.
    - For the Harper-revisited study it may be good to start from existing no-BP historical/spinup --> restart files needed to be altered, or at least spinup be accelerated by reading cpool-file, i.e. Tam's approaches towards modifying cpool-init-file may come handy.
    - definitely in ESM mode, likely also offline

## Steps for "Harper revisited"
Land use scenario

    - To do: check the original LUH2 data for CMIP6 (there is a Readme with it) -- it should contain some information on biomass plantations.
    - Different from Dorothea's study the areas are prescribed from the LUH forcing, not to match abandoned reforested regions, so the LU-with-BP scenario implementation should be simpler. The matrix still needs to be extended by BPs though.

## Simulation setup

Needs to be thought through once the model is running. Basic idea is two runs starting from today (~0 BPs area today) and running until 2100, planting (1) "proper" BPs or (2) grass/crop on the same areas.
