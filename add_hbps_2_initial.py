#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Edit a jsbach.nc file to contain an extra tile containing herbaceous biomass plantations.

Created on Mon Dec 10 14:38:07 2018

@author: tammas
"""

import netCDF4 as nc
import numpy as np
import os

# Load data
#os.chdir('/home/tammas/mistral/hbp-cmip6-merge2/experiments/offline/input')
os.chdir('/home/mpim/m300719/hbp-cmip6-merge2/experiments/offline/input')
os.system('cdo -delname,cover_type,cover_fract,natural_veg jsbach_T63GR15_11tiles_5layers_1850_dynveg.nc jsbach_12tiles_23lct.nc')
ncinit  = nc.Dataset('jsbach_T63GR15_11tiles_5layers_1850_dynveg.nc', 'r')
cover_type = ncinit.variables['cover_type'][:]
cover_fract = ncinit.variables['cover_fract'][:]
natural_veg = ncinit.variables['natural_veg'][:]
lat = ncinit.variables['lat'][:]

# Conactenate extra data onto the ntiles dimension for relevant variables.
cover_type = np.concatenate((cover_type,cover_type[None,-1,...]),axis=0)
cover_fract = np.concatenate((cover_fract,cover_fract[None,-1,...]),axis=0)
natural_veg = np.concatenate((natural_veg,natural_veg[None,-1,...]),axis=0)

# cover_type = 23/22
# Set all land (>0) gridpoints for tile 12 to 23 (23 = extratropical HBPs)
cover_type[-1,cover_type[-1,...]>0] = 23
# Subtract 1 for all gridpoints in the tropics (22 = tropical HBPs)
cover_type[-1,(lat>-30)&(lat<30),:] -= 1
# Return ocean gridpoints to 0
tropical_ocean = cover_type[-1,...]<0
cover_type[-1,tropical_ocean] = 0

# cover_fraction should start at 1e-10 and HBP are not natural vegetaton
cover_fract[-1,...] = 1e-10
ocean = cover_fract[-2,...]==0
cover_fract[-1,ocean] = 0
# Cover fraction where glacier=1 should be 0
glacier = cover_fract[0,...]==1
cover_fract[-1,glacier] = 0

# HBP are not natural vegetaton so use crop values for natural_veg
natural_veg[-1,...] = natural_veg[-2,...]

# Rescale cover fractions. JSBACH will do this by default but it's done here as a precaution.
cover_fract = cover_fract/cover_fract.sum(axis=0)
cover_fract[cover_fract.mask] = 0 # fill zero divisions with 0 again

# create the output netcdf variables
ncout = nc.Dataset('jsbach_12tiles_23lct.nc','r+')
ncout.createDimension('ntiles',size=12)
ncout.nlct = 23
ctype = ncout.createVariable('cover_type',ncinit.variables['cover_type'].dtype,dimensions=ncinit.variables['cover_type'].dimensions)
ctype.long_name = "land cover type"
ctype.units = "1"
ctype.lct01 = "glacier"
ctype.lct02 = "tropical broadleaf evergreen"
ctype.lct03 = "tropical broadleaf deciduous"
ctype.lct04 = "extra-tropical evergreen"
ctype.lct05 = "extra-tropical deciduous"
ctype.lct06 = "temperate broadleaf evergreen"
ctype.lct07 = "temperate broadleaf deciduous"
ctype.lct08 = "evergreen coniferous"
ctype.lct09 = "deciduous coniferous"
ctype.lct10 = "raingreen shrubs"
ctype.lct11 = "deciduous shrubs"
ctype.lct12 = "C3 grass"
ctype.lct13 = "C4 grass"
ctype.lct15 = "C3 pasture"
ctype.lct16 = "C4 pasture"
ctype.lct17 = "tundra"
ctype.lct18 = "swamp"
ctype.lct19 = "crops"
ctype.lct20 = "C3 crops"
ctype.lct21 = "C4 crops"
ctype.lct22 = "Tropical HBPs"
ctype.lct23 = "Extra-tropical HBPs"
cfrac = ncout.createVariable('cover_fract',ncinit.variables['cover_fract'].dtype,dimensions=ncinit.variables['cover_fract'].dimensions)
cfrac.long_name = "land cover fraction"
cfrac.units = "1"
natve = ncout.createVariable('natural_veg',ncinit.variables['natural_veg'].dtype,dimensions=ncinit.variables['natural_veg'].dimensions)
natve.long_name = "natural (potential) vegetation" ;
natve.units = "1" ;

# Insert data into netcdf variables
ctype[:] = cover_type
cfrac[:] = cover_fract
natve[:] = natural_veg

# Close
ncout.close()