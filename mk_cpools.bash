#/bin/bash
# Make a Cpools init file for jsbach with 12 tiles using an 11 tile file. 

# List of variables that have the tiles dimensions
vars=cover_fract,cover_fract_pot,Cpool_green,Cpool_reserve,Cpool_woods,Cpool_crop_harvest,frac_litter_wood_new,LAI_previousDayMean,act_fpc,max_green_bio,pot_fpc,ave_npp5,prev_year_soiltemp,prev_year_soilmoist,prev_year_gdd,prev_year_npp,gdd_upper_tlim,frac_1hr_wood,frac_10hr_wood,frac_100hr_wood,frac_1000hr_wood,YCpool_acid_ag1,YCpool_acid_bg1,YCpool_water_ag1,YCpool_water_bg1,YCpool_ethanol_ag1,YCpool_ethanol_bg1,YCpool_nonsoluble_ag1,YCpool_nonsoluble_bg1,YCpool_humus_1,YCpool_acid_ag2,YCpool_acid_bg2,YCpool_water_ag2,YCpool_water_bg2,YCpool_ethanol_ag2,YCpool_ethanol_bg2,YCpool_nonsoluble_ag2,YCpool_nonsoluble_bg2,YCpool_humus_2,Npool_green,Npool_woods,Npool_mobile,Npool_crop_harvest,Npool_litter_green_ag,Npool_litter_green_bg,Npool_litter_wood_ag,Npool_litter_wood_bg,Npool_slow,sminN_pool,NPP_run_mean

# Copy the variables for the 11th tile (-1 is crops for 11 tile files) with a record dimension 
cp /pool/data/JSBACH/cpools/T63/cpools_vga0218_18991231.nc .
ncks -O --mk_rec_dmn tiles -v $vars -d tiles,-1 cpools_vga0218_18991231.nc temp.nc

# Set cover fractions for HBP to 1e-10 # I don't think this is actually used by JSBACH.
ncap2 -O -s 'cover_fract*=0' -s 'cover_fract+=1e-10' temp.nc temp.nc

# Set carbon and nitrogen pools to 0
#ncap2 -O -s 'cover_fract*=0' temp.nc temp.nc
#ncap2 -O -s 'cover_fract_pot*=0' temp.nc temp.nc
ncap2 -O -s 'Cpool_green*=0' temp.nc temp.nc
ncap2 -O -s 'Cpool_reserve*=0' temp.nc temp.nc
#ncap2 -O -s 'Cpool_woods*=1' temp.nc temp.nc
ncap2 -O -s 'Cpool_crop_harvest*=0' temp.nc temp.nc
#ncap2 -O -s 'frac_litter_wood_new*=1' temp.nc temp.nc
#ncap2 -O -s 'LAI_previousDayMean*=1' temp.nc temp.nc
#ncap2 -O -s 'act_fpc*=1' temp.nc temp.nc
#ncap2 -O -s 'max_green_bio*=1' temp.nc temp.nc
#ncap2 -O -s 'pot_fpc*=1' temp.nc temp.nc
ncap2 -O -s 'ave_npp5*=0' temp.nc temp.nc
#ncap2 -O -s 'prev_year_soiltemp*=1' temp.nc temp.nc
#ncap2 -O -s 'prev_year_soilmoist*=1' temp.nc temp.nc
#ncap2 -O -s 'prev_year_gdd*=1' temp.nc temp.nc
ncap2 -O -s 'prev_year_npp*=0' temp.nc temp.nc
#ncap2 -O -s 'gdd_upper_tlim*=1' temp.nc temp.nc
#ncap2 -O -s 'frac_1hr_wood*=0' temp.nc temp.nc
#ncap2 -O -s 'frac_10hr_wood*=0' temp.nc temp.nc
#ncap2 -O -s 'frac_100hr_wood*=0' temp.nc temp.nc
#ncap2 -O -s 'frac_1000hr_wood*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_acid_ag1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_acid_bg1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_water_ag1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_water_bg1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_ethanol_ag1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_ethanol_bg1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_nonsoluble_ag1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_nonsoluble_bg1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_humus_1*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_acid_ag2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_acid_bg2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_water_ag2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_water_bg2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_ethanol_ag2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_ethanol_bg2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_nonsoluble_ag2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_nonsoluble_bg2*=0' temp.nc temp.nc
ncap2 -O -s 'YCpool_humus_2*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_green*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_woods*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_mobile*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_crop_harvest*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_litter_green_ag*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_litter_green_bg*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_litter_wood_ag*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_litter_wood_bg*=0' temp.nc temp.nc
ncap2 -O -s 'Npool_slow*=0' temp.nc temp.nc
ncap2 -O -s 'sminN_pool*=0' temp.nc temp.nc
ncap2 -O -s 'NPP_run_mean*=0' temp.nc temp.nc

# Create a file to append to that contains a record dimension
ncks -O --mk_rec_dmn tiles cpools_vga0218_18991231.nc temp2.nc

# Concatenate 
ncrcat -O temp2.nc temp.nc Cpools_12tiles.nc

# Clean up
rm temp.nc temp2.nc
